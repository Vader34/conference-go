import requests
from .keys import PEXELS_API_KEYS

def get_picture_url(query):
    url =f"https://api.pexels.com/v1/search?query={query}"

    headers ={
        "Authorization": PEXELS_API_KEYS,
    }
    response = requests.get(url, headers=headers)
    return response
